let s:statedir	= $XDG_STATE_HOME .. '/vim'

call mkdir(s:statedir .. '/backup', 'p')
call mkdir(s:statedir .. '/swap', 'p')
call mkdir(s:statedir .. '/undo', 'p')

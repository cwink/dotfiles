nnoremap <silent><leader>cc :call ToggleQuickFix()<cr>
nnoremap <silent><leader>cn :cn<cr>
nnoremap <silent><leader>cp :cp<cr>

nnoremap <silent><leader>m :make<cr>:cwindow<cr>:redraw!<cr>

nnoremap <silent><leader>d :bd!<cr>
nnoremap <silent><leader>n :bn!<cr>
nnoremap <silent><leader>p :bp!<cr>

nnoremap <silent><leader>q :qa!<cr>
nnoremap <silent><leader>w :wa!<cr>

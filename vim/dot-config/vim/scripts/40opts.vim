set backup
set enc=utf-8
set list
set mouse=
set nu
set swf
set tw=79

if has('cscope') && has('quickfix')
	set csqf=s-,c-,d-,i-,t-,e-,a-
	set cst
en

if has('persistent_undo')
	set udf
en

if has('syntax')
	set spell
	set spelllang=en_us
en

if has('termguicolors')
	set tgc
en

let s:confdir	= $XDG_CONFIG_HOME .. '/vim'
let s:statedir	= $XDG_STATE_HOME .. '/vim'

let &g:rtp	.= ',' .. s:confdir .. ',' .. s:confdir .. '/after'

let &g:bdir	 = s:statedir .. '/backup'
let &g:dir	 = s:statedir .. '/swap'
let &g:pp	 = &g:rtp
let &g:udir	 = s:statedir .. '/undo'
let &g:vi	.= ',n' .. s:statedir .. '/viminfo'

let g:netrw_home	= s:statedir

fu! ToggleQuickFix() abort
	if empty(filter(getwininfo(), 'v:val.quickfix'))
		cope
	el
		ccl
	en
endfunction

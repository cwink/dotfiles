if exists("b:curcomp")
	finish
en

let b:curcomp	= "mandoc"

CompilerSet makeprg=mandoc\ -T\ lint\ %
CompilerSet errorformat=mandoc:\ %f:%l:%c:\ %m

if exists("b:curcomp")
	finish
en

let b:curcomp	= "gawk"

CompilerSet makeprg=gawk\ --lint\ --posix\ -f\ %\ <'/dev/null'

if exists("b:didftplugin")
	finish
en

let b:didftplugin	= 1

compiler gcc

if filereadable('cscope.out')
	cs add cscope.out
en

#! /bin/sh

usage()
{
	cat <<-EOF
		usage: install.sh [-h] [-u]
		
		install dotfiles configuration
		
		        -h      display help information
		        -u      uninstall configuration
	EOF
	exit "${1}"
}

set -eu

uninstall='false'
while getopts 'hu' opt; do
	case "${opt}" in
	'u')
		uninstall='true'
		;;
	'?')
		usage 1 1>&2
		;;
	*)
		usage 0
		;;
	esac
done
shift $((OPTIND - 1))

. './profile/dot-config/profile.d/10xdg.sh'

osid="$(grep '^ID=' <'/etc/os-release' | awk -F'=' '{ print $2 }')"

if [ "${osid}" = 'alpine' ]; then
	apk -U add			\
		curl			\
		feh			\
		font-terminus		\
		gawk			\
		less			\
		mesa-dri-gallium	\
		shellcheck		\
		stow			\
		vim			\
		vimdiff			\
		xf86-input-libinput	\
		xinit			\
		xorg-server
fi

mkdir -p	"${XDG_BIN_HOME}"	\
		"${XDG_CACHE_HOME}"	\
		"${XDG_CONFIG_HOME}"	\
		"${XDG_DATA_HOME}"	\
		"${XDG_STATE_HOME}"

if [ ! -d "${XDG_RUNTIME_DIR}" ]; then
	uid="$(id -u)"
	gid="$(id -g)"
	doas install -dg "${gid}" -o "${uid}" -m 0700 "${XDG_RUNTIME_DIR}"
fi

rm -fr	"${HOME}/.gitconfig"	\
	"${HOME}/.lesshst"	\
	"${HOME}/.vim"		\
	"${HOME}/.viminfo"	\
	"${HOME}/.vimrc"

touch "${XDG_STATE_HOME}/lesshst"

for cfg in */; do
	if "${uninstall}"; then
		stow -S "${cfg}" --dotfiles -t "${HOME}"
	else
		stow -D "${cfg}" --dotfiles -t "${HOME}"
	fi
done

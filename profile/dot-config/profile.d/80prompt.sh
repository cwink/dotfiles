_getprompt()
{
	__bran="$(git branch --show-current 2>'/dev/null' | tr -d '[:space:]')"
	__cbran='\[\e[96m\]'
	__chost='\[\e[94m\]'
	__cnone='\[\e[0m\]'
	__cpwd='\[\e[95m\]'
	__cuser='\[\e[94m\]'

	printf '\n%s\u%s@%s\h%s:%s\w '	\
		"${__cuser}"		\
		"${__cnone}"		\
		"${__chost}"		\
		"${__cnone}"		\
		"${__cpwd}"
	if [ -n "${__bran}" ]; then
		printf '%s(%s%s%s)'	\
			"${__cnone}"	\
			"${__cbran}"	\
			"${__bran}"	\
			"${__cnone}"
	else
		printf '%s' "${__cnone}"
	fi
	printf '\n\$ '

	unset __bran __cbran __chost __cnone __cpwd __cuser
}

PS1='$(_getprompt)'

export PS1

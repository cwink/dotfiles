README(7) - Miscellaneous Information Manual

# NAME

**dotfiles** - system configuration

# DESCRIPTION

This repository contains the configuration that I use across all my
systems.
The
'`install.sh`'
script utilizes
[Stow](https://www.gnu.org/software/stow/)
to activate each applications configuration.
If the host is Alpine Linux, then the tools required for each configuration is
installed, including
'`stow`'.

# INSTALLATION

To install the configuration, use the
'`install.sh`'
script.
Run it with the
'`-h`'
argument to get more information.

## MICROSOFT TERMINAL

The following is a JSON snippet of the
[Zaibatsu](https://github.com/vim/vim/blob/master/runtime/colors/zaibatsu.vim)
color theme that I translated for the
[Microsoft Terminal](https://github.com/microsoft/terminal).

	{
		"background":		"#0E0024",
		"black":		"#0E0024",
		"blue":			"#5F00D7",
		"brightBlack":		"#0E0024",
		"brightBlue":		"#AFAFFF",
		"brightCyan":		"#87FFFF",
		"brightGreen":		"#87FF00",
		"brightPurple":		"#FFAFFF",
		"brightRed":		"#FF5FAF",
		"brightWhite":		"#FFFFFF",
		"brightYellow":		"#FFFF5F",
		"cursorColor":		"#FFFF5F",
		"cyan":			"#87FFFF",
		"foreground":		"#FFFFFF",
		"green":		"#5FAF00",
		"name":			"Zaibatsu",
		"purple":		"#D700FF",
		"red":			"#510039",
		"selectionBackground":	"#5FD7FF",
		"white":		"#D7D7D7",
		"yellow":		"#FFAF00"
	}

Place it in the
*schemes*
array of the settings file.

Linux 6.6.37-1-virt - July 25, 2024
